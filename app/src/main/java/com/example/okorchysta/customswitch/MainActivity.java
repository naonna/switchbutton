package com.example.okorchysta.customswitch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Switch s = new Switch(this);
        CheckBox checkBox;

        SwitchButton switchButton = findViewById(R.id.switchButton);
        switchButton.setTrackSize(600, 20);

    }
}