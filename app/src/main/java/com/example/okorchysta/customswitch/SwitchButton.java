package com.example.okorchysta.customswitch;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


public class SwitchButton extends AppCompatCheckBox implements View.OnClickListener {

    private static final String TAG = "SwitchButton";

    boolean isTurnedOn = false;


    private int thumbHeight;
    private int thumbWidth;
    private int thumbOnColor;
    private int thumbOffColor;

    private int trackHeight;
    private int trackWidth;
    private float trackRadius;
    private int trackOnColor;
    private int trackOffColor;

    private LayerDrawable switchOn;
    private LayerDrawable switchOff;


    public SwitchButton(Context context) {
        super(context);
    }

    public SwitchButton(Context context, AttributeSet attrs) {
        super(context, attrs,  R.attr.switchStyle);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.SwitchButton, 0, R.style.SwitchButton);
        try {
            thumbHeight = (int) ta.getDimension(R.styleable.SwitchButton_thumbHeight, 0);
            thumbWidth = (int) ta.getDimension(R.styleable.SwitchButton_thumbWidth, 0);

            trackHeight = (int) ta.getDimension(R.styleable.SwitchButton_trackHeight, 0);
            trackWidth = (int) ta.getDimension(R.styleable.SwitchButton_trackWidth, 0);
            trackRadius = ta.getDimension(R.styleable.SwitchButton_trackRadius, 0);

            thumbOnColor = ta.getColor(R.styleable.SwitchButton_thumbOnColor, 0);
            thumbOffColor = ta.getColor(R.styleable.SwitchButton_thumbOffColor, 0);
        } finally {
            ta.recycle();
        }

        TransitionDrawable transition = (TransitionDrawable) ContextCompat.getDrawable(context, R.drawable.transition);
        setBackground(transition);

        switchOn = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.switch_on);
        switchOff = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.switch_off);

        setTrackSize(trackWidth, trackHeight, trackRadius);
        setThumbSize(thumbWidth, thumbHeight);
        setTrackOffColor(trackOffColor);
        setTrackOnColor(trackOnColor);
        setThumbOffColor(thumbOffColor);
        setThumbOnColor(thumbOnColor);

        isTurnedOn = isChecked();
        if (isInEditMode()){
            if (isChecked())
                setBackground(switchOn);
            else
                setBackground(switchOff);
            return;
        }

        setSwitchBackground();

        setOnClickListener(this);
    }

    public void setSwitchBackground() {
        TransitionDrawable transition = (TransitionDrawable) getBackground();
        if (isTurnedOn){
            transition.startTransition(0);
        }
        else{
            transition.resetTransition();
        }

    }

    public void setThumbSize(int width, int height){
        thumbHeight = height;
        thumbWidth = width;

        setDrawableSize((GradientDrawable) switchOff.findDrawableByLayerId(R.id.thumb_off), width, height);
        setDrawableSize((GradientDrawable) switchOn.findDrawableByLayerId(R.id.thumb_on), width, height);

        invalidate();
    }

    public void setTrackSize(int width, int height){
        trackHeight = height;
        trackWidth = width;

        setDrawableSize((GradientDrawable) switchOn.findDrawableByLayerId(R.id.track_on), width, height);
        setDrawableSize((GradientDrawable) switchOff.findDrawableByLayerId(R.id.track_off), width, height);

        invalidate();
    }


    public void setTrackSize(int width, int height, float radius){
        setTrackSize(width, height);
        trackRadius = radius;

        ((GradientDrawable) switchOn.findDrawableByLayerId(R.id.track_on)).setCornerRadius(radius);
        ((GradientDrawable) switchOff.findDrawableByLayerId(R.id.track_off)).setCornerRadius(radius);

        invalidate();
    }

    public void setThumbOnColor(int color){
         if (color!=0){
            thumbOnColor = color;
            setColor((GradientDrawable ) switchOn.findDrawableByLayerId(R.id.thumb_on), thumbOnColor);
        }
    }

    public void setThumbOffColor(int color){
        if (color!=0){
            thumbOffColor = color;
            setColor((GradientDrawable) switchOff.findDrawableByLayerId(R.id.thumb_off), thumbOffColor);
        }
    }

    public void setTrackOnColor(int color){
        if (color!=0){
            trackOnColor = color;
            setColor((GradientDrawable) switchOff.findDrawableByLayerId(R.id.track_on), color);
        }
    }

    public void setTrackOffColor(int color){
        if (color!=0){
            trackOffColor = color;
            setColor((GradientDrawable) switchOff.findDrawableByLayerId(R.id.track_off), color);
        }
    }

    private void setColor(GradientDrawable drawable, int color){
        drawable.setColor(color);
        invalidate();
    }

    private void setDrawableSize(GradientDrawable drawable, int width, int height){
        drawable.setSize(width, height);
        drawable.setBounds(0,0, width, height);
    }

    @Override
    public void onClick(View v) {
        makeAnimation();
        isTurnedOn = !isTurnedOn;
        setChecked(isTurnedOn);
    }

    private void makeAnimation(){
        TransitionDrawable transition = (TransitionDrawable) getBackground();
        transition.setCrossFadeEnabled(true);
        if (isTurnedOn)
            transition.reverseTransition(50);
        else
            transition.startTransition(50);
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putBoolean("isTurnedOn", isTurnedOn);
        Log.d(TAG, "onSaveInstanceState: ");
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            isTurnedOn = bundle.getBoolean("isTurnedOn");
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState(state);
    }
}
